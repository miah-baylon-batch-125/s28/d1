let http = require("http");
const PORT = 3000;

let directory = [
	{
		"name": "Brandon",
		"email": "brandon@mail.com"
	},
	{
		"name": "Robert",
		"email": "robert@mail.com"
	}
	];

http.createServer( (req, res) =>
 //retrieve our documents
 	//url = /user
 	//method = GET
 	//response = array of objects
 {
 	if (req.url === "/user" && req.method ===  "GET") {
 		res.writeHead( 200, 
			{"Content-Type": "application/json"}
		);

		res.write(JSON.stringify(directory));
		res.end();
	} 

//create new user
	//url = /user
	//method = Post
	//data from the request will be coming 

	if (req.url === "/user" && req.method === "POST") {
		
			let reqBody = "";
			req.on("data", (data) => {
				reqBody += data;
		});
			req.on("end", () => {
				console.log(typeof reqBody);
				reqBody = JSON.parse(reqBody)
				console.log(reqBody);

				let newUser = {
					"name": reqBody.name,
					"email": reqBody.email
				}

				directory.push(newUser);
				console.log(directory)


				res.writeHead( 200, 
					{"Content-Type": "application/json"});

				res.write(JSON.stringify(directory));
				res.end();

			})
	}



 }).listen(PORT);

 console.log(`Server is now connected to port ${PORT}`)